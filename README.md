
# README

![logo\_enlazadas](logo_enlazadas.png)

## Introducción

#### ¿Qué es?

El proyecto Enlazadas aspira a reunir en un mismo lugar información de
diversa naturaleza que pueda ser de utilidad a los enfermos de cáncer de
mama. Así mismo, pretende ser una herramienta que conecte a sus usuarios
(ya sean pacientes, familiares y allegados o asociaciones relacionadas
con esta enfermedad) entre sí para ayudar a crear una comunidad que dé
apoyo durante esa etapa tan difícil.

#### ¿ Cómo funciona?

La aplicación web consta de varias secciones. La primera, “Mapa”,
permite al usuario encontrar lugares de interés o eventos cercanos
utilizando filtros personalizados. En “Eventos” se pueden encontrar,
dentro de unas fechas a elegir, información sobre conferencias,
encuentros deportivos, reuniones, campañas y actividades varias. La
tercera sección, “Comunidad”, da acceso a una serie de foros atendiendo
al tipo de perfil del usuario, dando la posibilidad de que encuentre
gente en su misma situación con la que compartir experiencias.

#### Respuesta al reto y beneficios a los usuarios

El objetivo principal de esta app es poner en contacto al usuario con
todos los recursos disponibles a su alrededor que puedan ayudar en todo
lo relacionado con la enfermedad. Facilitando el acceso a la información
y a encontrar a otras personas en su misma situación se espera conseguir
aliviar en cierta medida la difícil situación en la que se encuentran.
Permitirá, por ejemplo, incentivar un estilo de vida saludable,
encontrar comunidades de apoyo y acompañamiento donde compartir
experiencias.

#### Desarrollo técnico

La solución al reto es una aplicación basada en web. Para su desarrollo
frontend se ha utilizado la tecnología React basada en Javascript junto
con las siguientes librerías: material- ui, typescript, google-map-react
y dot-env entre otras. Todos estos componentes son de código abierto. El
frontend conecta con Firebase para recibir información de la base de
datos Firestore Database. Para esta primera iteración hemos creado una
tabla que contiene los pin que se agregan al mapa en el frontend, los
cuales podemos filtrar según diferentes campos.

#### ¿Qué se encuentra en este repositorio?

El proyecto desarrollado durante este Hacking for Humanity 2021 por el
Equipo 3 se presenta de dos formas. Por un lado se enseña un prototipo
para mostrar el potencial de la idea, y por otro se muestra la
aplicación que ha dado tiempo a programar en el tiempo establecido.

## El prototipo de la aplicación

El prototipo montado muestra cinco pantallas: las tres principales
(mapa, eventos y comunidad) más una de portada y otra de contacto.

#### Portada

Estar sería la pantalla de inicio. El diseño es sencillo para dejar
claro al usuario las tres principales herramientas que le aporta la
aplicación. Una mejora pendiente sería añadir un poco más de
información, una presentación que indique lo que la aplicación va a
aportar al usuario.

![pantalla\_portada](pantalla_portada.png)

#### Primera pantalla: Mapa

En esta segunda se incluye un mapa (que se puede centrar en el lugar de
elección del usuario) con chinchetas en los puntos de interés. Estas
chinchetas están clasificadas en distintas categorías y se pueden
filtrar a gusto del usuario. Incluyen servicios como los centros de
salud más cercanos, las consultas de psicólogos que puedan ayudar con la
parte emocional del cáncer o fisioterapeutas que hagan rehabilitación.

Además, también aparecen eventos que tengan lugar dentro de unas fechas
establecidas, como pueden ser eventos deportivos, charlas o
comunicaciones y campañas promovidas por distintas asociaciones para
recaudar fondos por la causa.

Por último, en esta aplicación también queremos informar a aquellos que
quieran aportar su granito de arena, indicándoles puntos donde donar
pelo o sangre o dónde apuntarse a asociaciones.

![pantalla\_mapa](pantalla_mapa.png)

#### Segunda pantalla: Eventos

Los eventos que se pueden encontrar en el mapa de la primera pestaña se
encuentran más detallados en esta segunda página. Ya sean eventos
deportivos, comunicaciones, campañas o cualquier otro tipo de
acontecimiento de interés, aparecerán filtrados de acuerdo a las fechas
que se indiquen y a otros parámetros como si son presenciales u online,
o su precio. En cada entrada se podrá encontrar información de cada
evento, y habrá un link a la página o asociación que lo organice.

![pantalla\_evento](pantalla_evento.png)

#### Tercera pantalla: Comunidad

Las dos primeras pantallas de esta aplicación están pensadas para
informar a cualquiera que se interese por estos temas. Esta tercera, sin
embargo, está enfocada a aquellos que viven de cerca el cáncer de mama.
Registrándose con un perfil de paciente, familiar o asociación se
entrará a distintos foros privados a los que solo tendrá acceso gente en
las mismas circunstancias. Se pretende dar así un sitio seguro donde
conectar con otras personas.

Al registrarse como paciente, se accederá a un foro con los siguientes
temas:

-   Testimonios: donde quien lo desee comparta sus experiencias, sus
    vivencias o recomendaciones.

-   Equipos regionales: donde descubrir qué otros pacientes con esta
    patología pueden encontrarse cerca del usuario, lo que les permitirá
    reunirse presencialmente si así lo deseasen.

-   Quedadas deportivas: donde planificar reuniones para hacer deporte,
    algo que está demostrado que ayuda a fortalecer el cuerpo y la mente
    de enfermos y no enfermos. El quedar con gente en la misma situación
    ayudará a motivar y creará sensación de comunidad.

-   Desafíos: aquí los usuarios podrán planear desafíos que imponerse
    para tratar de mejorar su estilo de vida, siempre supervisados por
    algún profesional.

Como sabemos que los familiares o amigos más cercanos de un paciente con
cáncer también pasarán por una etapa difícil, queremos darles la
oportunidad de tener voz y encontrarse con otros familiares que puedan
sufrir sus mismas dificultades. Tendrán acceso a foros con los
siguientes temas:

-   Testimonios: similar al que tienen los usuarios pacientes. Será un
    lugar de desahogo para ellos.

-   Familias con hijos: entorno específico para hablar de cómo manejar
    las diversas situaciones que se dan al tener hijos con una pareja
    con cáncer. También permitirá hacer quedadas.

-   Carpooling: aquí se podrán organizar para compartir coche si les
    coinciden viajes al hospital o los centros de salud.

-   Reutilización de material: si necesitan algún tipo de material
    médico (por ejemplo muletas o sillas de ruedas) podrán buscar gente
    a su alrededor que en su momento lo haya usado pero que ya no lo
    necesite.

Finalmente, también queremos ofrecer un lugar para que las diversas
asociaciones que trabajen contra el cáncer de mama tengan su voz y
puedan colaborar entre ellas. Así mismo, aquí también se les permitiría
proporcionar información para completar la existente en la web.

![pantalla\_comunidad](pantalla_comunidad.png)

#### Cuarta pantalla: Contacto

Finalmente, y como somos conscientes de que el usuario puede dar
valoraciones o ideas que enriquezcan y ayuden a crecer el proyecto, en
está página les damos la oportunidad de ponerse en contacto con la
organización.

![pantalla\_contacto](pantalla_contacto.png)

## La aplicación

Dado el breve espacio de tiempo disponible para el desarrollo del
proyecto, no se ha podido llegar a programar todo lo planteado en el
prototipo. En este gif se muestra la parte de la aplicación funcional
que se programó, centrada en el mapa con las chinchetas de puntos de
interés, con los filtros indicados.

![demo](demo.gif)

## Futuro desarrollo del proyecto

Somos conscientes de que un proyecto como este siempre tiene marjen de
mejora y desarrollo. Aquí planteamos algunas ideas que se podrían
incluir en etapas posteriores:

-   Tener disponible tanto una versión en página web como una aplicación
    descargable en móvil o tablet.

-   Añadir más idiomas: inglés, gallego, euskera y catalán.

-   Dar la posibilidad a los usuarios registrados a que personalicen su
    propio mapa, para que puedan tener siempre activos los filtros que
    más les interesen.

-   Permitir a los usuarios registrados completar su propio calendario
    con los eventos recogidos en la aplicación, con las reuniones que
    acuerden con otros usuarios o con las citas médicas que tengan
    programadas.

-   Abrirse a la posibilidad de extender este proyecto a otros tipos de
    cáncer.

-   Por último, pero no menos importante, contactar con personas de cada
    uno de los tres perfiles aquí recogidos (pacientes, familiares y
    allegados o asociaciones) para que nos sugieran, desde su
    experiencia, cambios y mejoras que adoptar.
