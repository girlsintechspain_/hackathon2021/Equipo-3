import SearchIcon from "@mui/icons-material/Search";
import { InputBase } from "@mui/material";
import { styled, alpha } from "@mui/material/styles";
import { useState } from "react";

const BuscarCerca = ({ setCentrarMadrid }: { setCentrarMadrid: any }) => {
  const [busqueda, setBusqueda] = useState("");

  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.primary.main, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.primary.main, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("md")]: {
        width: "20ch",
      },
    },
  }));

  const submitSearch = () => {
    //Busqueda estatica para demo
    setCentrarMadrid();
  };

  return (
    <Search>
      <SearchIconWrapper>
        <SearchIcon />
      </SearchIconWrapper>
      <StyledInputBase
        value={busqueda}
        placeholder="Busca cerca de ti..."
        inputProps={{ "aria-label": "search" }}
        onChange={(e) => setBusqueda(e.target.value)}
        onKeyPress={(e) => (e.key === "Enter" ? submitSearch() : null)}
      />
    </Search>
  );
};

export default BuscarCerca;
