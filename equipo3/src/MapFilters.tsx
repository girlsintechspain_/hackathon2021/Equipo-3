import {
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
  Checkbox,
  FormControlLabel,
  FormGroup,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import theme from "./styles/theme";

const MapFilters = () => {
  return (
    <div style={{ minHeight: "20vh" }}>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          style={{ background: `${theme.palette.primary.main}` }}
        >
          <Typography color="white">Servicios</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <FormGroup>
              <FormControlLabel
                control={<Checkbox />}
                label="Centros de salud"
              />
              <FormControlLabel control={<Checkbox />} label="Psicólogos" />
              <FormControlLabel
                control={<Checkbox />}
                label="Fisioterapeutas"
              />
            </FormGroup>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
          style={{ background: `${theme.palette.primary.main}` }}
        >
          <Typography color="white">Eventos</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <FormGroup>
              <FormControlLabel control={<Checkbox />} label="Deportivos" />
              <FormControlLabel control={<Checkbox />} label="Charlas" />
              <FormControlLabel
                control={<Checkbox />}
                label="Recaudación de fondos"
              />
            </FormGroup>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
          style={{ background: `${theme.palette.primary.main}` }}
        >
          <Typography color="white">¿Quieres ayudar?</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <FormGroup>
              <FormControlLabel control={<Checkbox />} label="Asociaciones" />
              <FormControlLabel control={<Checkbox />} label="Dona pelo" />
              <FormControlLabel control={<Checkbox />} label="Dona sangre" />
            </FormGroup>
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default MapFilters;
