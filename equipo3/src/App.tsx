import "./styles/App.css";
import Layout from "./Layout";

function App() {
  require("dotenv").config();
  return (
    <div className="App">
      <Layout/>
    </div>
  );
}

export default App;
