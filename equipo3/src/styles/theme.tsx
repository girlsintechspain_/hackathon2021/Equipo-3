import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      light: '#f381a7',
      main: '#f06292',
      dark: '#a84466',
      contrastText: '#fff',
    },
    secondary: {
      light: '#99d5cf',
      main: '#80cbc4',
      dark: '#598e89',
      contrastText: '#000',
    },
  },
});

export default theme;