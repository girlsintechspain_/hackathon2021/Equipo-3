import { collection, getDocs, query, where } from "@firebase/firestore";
import { Tabs, Tab } from "@mui/material";
import { Box } from "@mui/system";
import { useState, useEffect } from "react";
import { db } from "./firebase-config";
import VistaMapa from "./VistaMapa";
import theme from "./styles/theme";
import TabPanel from "./TabPanels";

const Layout = () => {
  const [chinchetas, setChinchetas] = useState<any>([]);
  const chinchetasRef = collection(db, "Chinchetas"); //Coleccion en Firebase db
  const [value, setValue] = useState(0);
  const [filtrar, setFiltrar] = useState(false);

  const handleChange = (event: any, newValue: any) => {
    setValue(newValue);
  };

  useEffect(() => {
    const getChinchetas = async () => {
      const data = await getDocs(chinchetasRef);
      setChinchetas(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getChinchetas();
  }, []);

  useEffect(() => {
    const getChinchetas = async () => {
      //Filtra por categoria psicologo y centro de salud (hardcoded para demo)
      const q = query(
        chinchetasRef,
        where("categoria", "in", ["Psicólogo", "Centro de Salud"])
      );
      const querySnapshot = await getDocs(q);
      setChinchetas(
        querySnapshot.docs.map((querySnapshot) => ({
          ...querySnapshot.data(),
          id: querySnapshot.id,
        }))
      );
    };

    if (filtrar) {
      getChinchetas();
    }
  }, [filtrar]);

  return (
    <Box sx={{ height: "50px", background: `${theme.palette.primary.main}` }}>
      <Tabs value={value} onChange={handleChange}>
        <Tab label="Mapa" />
        <Tab label="Eventos" />
        <Tab label="Comunidad" />
      </Tabs>
      <TabPanel value={value} index={0}>
        <VistaMapa chinchetas={chinchetas} filtrar={() => setFiltrar(true)} />
      </TabPanel>
      <TabPanel value={value} index={1}></TabPanel>
      <TabPanel value={value} index={2}></TabPanel>
    </Box>
  );
};

export default Layout;
