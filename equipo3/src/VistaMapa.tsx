import GoogleMapReact from "google-map-react";
import { useState } from "react";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import { Button } from "@mui/material";
import { Box } from "@mui/system";
import MapFilters from "./MapFilters";
import theme from "./styles/theme";
import Chincheta from "./Chincheta";
import BuscarCerca from "./BuscarCerca";

const VistaMapa = ({
  chinchetas,
  filtrar,
}: {
  chinchetas: any[];
  filtrar: any;
}) => {
  const mapsKey = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;
  const [open, setOpen] = useState(false);
  const [centrarMadrid, setCentrarMadrid] = useState(false);
  const madrid = {
    lat: 40.416775,
    lng: -3.70379,
  };

  const barcelona = {
    lat: 41.390205,
    lng: 2.154007,
  };

  const toggleDrawer = (newOpen: any) => () => {
    setOpen(newOpen);
  };

  const filtrarMapa = () => {
    //Mock filter que usa la base de datos Firebase
    filtrar();
    setOpen(false);
  };

  return (
    <div style={{ height: "70vh", width: "100%" }}>
      <Box sx={{ textAlign: "center", pt: 1 }}>
        <BuscarCerca setCentrarMadrid={() => setCentrarMadrid(true)} />
        <Button
          style={{ color: `${theme.palette.primary.dark}` }}
          onClick={toggleDrawer(true)}
        >
          Filtrar mapa
        </Button>
      </Box>
      <GoogleMapReact
        bootstrapURLKeys={{ key: mapsKey !== undefined ? mapsKey : "" }}
        center={centrarMadrid ? madrid : barcelona}
        defaultZoom={12}
      >
        {chinchetas !== undefined
          ? chinchetas.map((pin) => (
              <Chincheta
                lat={pin.ubicacion._lat}
                lng={pin.ubicacion._long}
                key={pin.id}
                elemento={pin}
              />
            ))
          : null}
      </GoogleMapReact>
      <SwipeableDrawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer(false)}
        onOpen={toggleDrawer(true)}
        swipeAreaWidth={56}
        disableSwipeToOpen={false}
        ModalProps={{
          keepMounted: true,
        }}
      >
        <MapFilters />
        <Button
          style={{
            background: `${theme.palette.primary.dark}`,
            marginTop: "10px",
            float: "right",
          }}
          variant="contained"
          onClick={filtrarMapa}
        >
          Aplicar filtros
        </Button>
      </SwipeableDrawer>
    </div>
  );
};

export default VistaMapa;
