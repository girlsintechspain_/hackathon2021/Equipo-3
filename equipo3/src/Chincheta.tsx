import DirectionsRunIcon from "@mui/icons-material/DirectionsRun";
import SvgIcon from "@mui/icons-material/DirectionsRun";
import RoomIcon from "@mui/icons-material/Room";
import RecordVoiceOverIcon from "@mui/icons-material/RecordVoiceOver";
import EuroSymbolRoundedIcon from "@mui/icons-material/EuroSymbolRounded";
import FaceIcon from "@mui/icons-material/Face";
import LocalHospitalIcon from "@mui/icons-material/LocalHospital";
import SpeakerNotesIcon from "@mui/icons-material/SpeakerNotes";

const Chincheta = (props: any) => {
  const categoria = props.elemento.categoria;
  switch (categoria) {
    case "Deportivo":
      return <SvgIcon component={DirectionsRunIcon} />;
    case "Conferencia":
      return <SvgIcon component={RecordVoiceOverIcon} />;
    case "Campaña":
      return <SvgIcon component={EuroSymbolRoundedIcon} />;
    case "Peluquería":
      return <SvgIcon component={FaceIcon} />;
    case "Centro de Salud":
      return <SvgIcon component={LocalHospitalIcon} />;
    case "Psicólogo":
      return <SvgIcon component={SpeakerNotesIcon} />;
  }

  return <SvgIcon component={RoomIcon} />;
};

export default Chincheta;
