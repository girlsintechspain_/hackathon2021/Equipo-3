var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1280" deviceHeight="800">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1635003111697.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1635003111697-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><![endif]-->\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
\
    <div id="s-119e43e7-c410-48dc-aa70-9f26d77649e4" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="PORTADA" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/119e43e7-c410-48dc-aa70-9f26d77649e4-1635003111697.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/screens/119e43e7-c410-48dc-aa70-9f26d77649e4-1635003111697-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/screens/119e43e7-c410-48dc-aa70-9f26d77649e4-1635003111697-ie8.css" /><![endif]-->\
      <div class="freeLayout">\
      <div id="s-Rectangle_6" class="pie rectangle manualfit firer click commentable non-processed" customid="Rectangle_6"   datasizewidth="340.0px" datasizeheight="416.0px" datasizewidthpx="340.0" datasizeheightpx="416.00000000000034" dataX="108.0" dataY="306.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_6_0">MAPA<br /><br /><br /><br /><br /><br /></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Rectangle_7" class="pie rectangle manualfit firer click commentable non-processed" customid="Rectangle_6"   datasizewidth="340.0px" datasizeheight="416.0px" datasizewidthpx="340.0" datasizeheightpx="416.00000000000034" dataX="470.0" dataY="306.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_7_0">EVENTOS<br /><br /><br /><br /><br /><br /></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Rectangle_8" class="pie rectangle manualfit firer click commentable non-processed" customid="Rectangle_6"   datasizewidth="340.0px" datasizeheight="416.0px" datasizewidthpx="339.9999999999999" datasizeheightpx="416.00000000000034" dataX="834.0" dataY="306.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_8_0">COMUNIDAD<br /><br /><br /><br /><br /><br /><br /></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Text_1" class="pie richtext autofit firer ie-background commentable non-processed" customid="Hacking for Humanity 2021"   datasizewidth="324.7px" datasizeheight="32.0px" dataX="123.0" dataY="97.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Text_1_0">Hacking for Humanity 2021</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Text_2" class="pie richtext autofit firer ie-background commentable non-processed" customid="#grupo 3"   datasizewidth="111.9px" datasizeheight="32.0px" dataX="1044.0" dataY="97.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Text_2_0"> #grupo 3</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph" class="pie richtext autofit firer ie-background commentable non-processed" customid="Ar&iacute;s Fanjul HeviaPaula Go"   datasizewidth="208.1px" datasizeheight="116.0px" dataX="952.9" dataY="151.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_0">Ar&iacute;s Fanjul Hevia<br />Paula Gonz&aacute;lez Carbajal<br />Karen Salazar Guti&eacute;rrez<br /><br /></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_1" class="pie richtext autofit firer ie-background commentable non-processed" customid="ENLAZADAS"   datasizewidth="282.1px" datasizeheight="65.0px" dataX="524.1" dataY="194.5" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_1_0">ENLAZADAS</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Image_25" class="pie image firer ie-background commentable non-processed" customid="Image_25"   datasizewidth="50.0px" datasizeheight="63.0px" dataX="253.0" dataY="550.0"   alt="image" systemName="./images/f65cd110-1ece-4dff-9d18-28ebf6f82445.svg" overlay="#CDD721">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="32px" version="1.1" viewBox="0 0 25 32" width="25px">\
          	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
          	    <title>Pins</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <defs />\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_25-Page-1" stroke="none" stroke-width="1">\
          	        <g fill="#282828" id="s-Image_25-Components" transform="translate(-770.000000, -928.000000)">\
          	            <g id="s-Image_25-Social" transform="translate(100.000000, 849.000000)">\
          	                <g id="s-Image_25-Pins" transform="translate(670.000000, 79.000000)">\
          	                    <g id="s-Image_25-Big-Pin">\
          	                        <path d="M12.5,0 C5.596,0 0,5.535 0,12.363 C0,21.79 10.117,32 12.541,32 C14.965,32 25,21.79 25,12.363 C25,5.535 19.404,0 12.5,0 L12.5,0 Z M12.5,18.093 C9.326,18.093 6.753,15.549 6.753,12.409 C6.753,9.27 9.326,6.725 12.5,6.725 C15.674,6.725 18.247,9.27 18.247,12.409 C18.247,15.549 15.674,18.093 12.5,18.093 L12.5,18.093 Z" style="fill:#CDD721 !important;" />\
          	                    </g>\
          	                </g>\
          	            </g>\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_13" class="pie image firer ie-background commentable non-processed" customid="Image_13"   datasizewidth="73.0px" datasizeheight="72.0px" dataX="603.5" dataY="545.5"   alt="image" systemName="./images/80bb03fe-d85e-47d3-a8cb-37ff9f3122c2.svg" overlay="#EA96E1">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="24px" version="1.1" viewBox="0 0 24 24" width="24px">\
          	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
          	    <title>Dribbble Icon</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <defs />\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_13-Page-1" stroke="none" stroke-width="1">\
          	        <g fill="#DDDDDD" id="s-Image_13-Components" transform="translate(-683.000000, -934.000000)">\
          	            <g id="s-Image_13-Social" transform="translate(100.000000, 849.000000)">\
          	                <g id="s-Image_13-Rounded-Icons" transform="translate(515.000000, 85.000000)">\
          	                    <g id="s-Image_13-Dribbble-Icon" transform="translate(68.000000, 0.000000)">\
          	                        <path d="M12,24 C5.372583,24 0,18.627417 0,12 C0,5.372583 5.372583,0 12,0 C18.627417,0 24,5.372583 24,12 C24,18.627417 18.627417,24 12,24 Z M18,12 C18,8.688 15.312,6 12,6 C8.688,6 6,8.688 6,12 C6,15.312 8.688,18 12,18 C15.312,18 18,15.312 18,12 Z M8.856,16.044 L8.736,15.948 C8.772,15.984 8.808,16.008 8.856,16.032 L8.856,16.044 Z M12.552,11.976 C10.848,12.468 9.048,13.896 8.184,15.42 L8.196,15.432 C7.344,14.484 6.876,13.26 6.876,12 L6.876,11.832 C8.58,11.88 10.524,11.592 12.144,11.112 C12.288,11.4 12.432,11.688 12.552,11.976 Z M14.004,16.716 C13.368,16.992 12.684,17.124 12,17.124 C10.86,17.124 9.756,16.74 8.856,16.032 C8.988,15.744 9.192,15.468 9.384,15.228 C10.308,14.076 11.484,13.32 12.876,12.84 L12.9,12.828 C13.392,14.076 13.776,15.396 14.004,16.716 Z M11.712,10.32 C10.224,10.716 8.628,10.956 7.08,10.944 L6.984,10.944 C7.32,9.372 8.364,8.052 9.816,7.368 C10.512,8.304 11.148,9.3 11.712,10.32 Z M17.064,12.816 C16.836,14.208 16.032,15.456 14.868,16.248 C14.652,15.012 14.292,13.776 13.86,12.588 C14.22,12.528 14.58,12.504 14.928,12.504 C15.636,12.504 16.392,12.6 17.064,12.816 Z M15.384,8.16 C14.856,8.976 13.584,9.684 12.708,10.008 C12.144,8.976 11.508,7.956 10.788,7.02 C11.184,6.924 11.592,6.876 12,6.876 C13.248,6.876 14.448,7.332 15.384,8.16 Z M17.124,11.952 C16.344,11.784 15.528,11.712 14.736,11.712 C14.34,11.712 13.944,11.736 13.548,11.784 C13.416,11.436 13.26,11.1 13.104,10.776 C14.028,10.392 15.372,9.6 15.96,8.748 C16.704,9.648 17.112,10.776 17.124,11.952 Z" style="fill:#EA96E1 !important;" />\
          	                    </g>\
          	                </g>\
          	            </g>\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_35" class="pie image firer ie-background commentable non-processed" customid="Image_35"   datasizewidth="70.0px" datasizeheight="80.9px" dataX="969.0" dataY="541.1"   alt="image" systemName="./images/e9931563-5ca5-417f-9c48-7a4d2b89f5b2.svg" overlay="#58A671">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="20px" version="1.1" viewBox="0 0 17 20" width="17px">\
          	    <!-- Generator: Sketch 52.1 (67048) - http://www.bohemiancoding.com/sketch -->\
          	    <title>user</title>\
          	    <desc>Created with Sketch.</desc>\
          	    <g fill="none" fill-rule="evenodd" id="s-Image_35-Page-1" stroke="none" stroke-width="1">\
          	        <g fill="#666666" id="s-Image_35-user">\
          	            <path d="M8.63492063,0 C5.80790159,0 3.50793651,2.29996508 3.50793651,5.12698413 C3.50793651,7.95400317 5.80790159,10.2539683 8.63492063,10.2539683 C11.4619397,10.2539683 13.7619048,7.95400317 13.7619048,5.12698413 C13.7619048,2.29996508 11.4619397,0 8.63492063,0" id="s-Image_35-Fill-1" style="fill:#58A671 !important;" />\
          	            <path d="M9.63332578,11.4682653 L7.36665911,11.4682653 C5.40191244,11.4682653 3.55087689,12.2442836 2.15461022,13.65341 C0.765181333,15.0556274 -7.55555556e-06,16.9065514 -7.55555556e-06,18.8653527 C-7.55555556e-06,19.1764059 0.253708,19.4285827 0.566659111,19.4285827 L16.4333258,19.4285827 C16.7462769,19.4285827 16.9999924,19.1764059 16.9999924,18.8653527 C16.9999924,16.9065514 16.2348036,15.0556274 14.8453747,13.65341 C13.449108,12.2442836 11.5981102,11.4682653 9.63332578,11.4682653 Z" id="s-Image_35-Fill-3" style="fill:#58A671 !important;" />\
          	        </g>\
          	    </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;