(function(window, undefined) {
  var dictionary = {
    "bb6a2cf1-cf7e-446c-838c-a667ec15a368": "EVENTOS",
    "f4a2babf-d3be-45cc-8019-b8dd67f27104": "CONTÁCTANOS",
    "119e43e7-c410-48dc-aa70-9f26d77649e4": "PORTADA",
    "fbff5bfe-ff9b-4aa4-8a51-b30ae33da3a8": "COMUNIDAD",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "MAPA",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);