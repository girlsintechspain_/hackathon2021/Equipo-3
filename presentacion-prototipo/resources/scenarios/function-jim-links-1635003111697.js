(function(window, undefined) {

  var jimLinks = {
    "bb6a2cf1-cf7e-446c-838c-a667ec15a368" : {
      "Rectangle_2" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Rectangle_4" : [
        "fbff5bfe-ff9b-4aa4-8a51-b30ae33da3a8"
      ],
      "Rectangle_5" : [
        "f4a2babf-d3be-45cc-8019-b8dd67f27104"
      ]
    },
    "f4a2babf-d3be-45cc-8019-b8dd67f27104" : {
      "Rectangle_2" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Rectangle_3" : [
        "bb6a2cf1-cf7e-446c-838c-a667ec15a368"
      ],
      "Rectangle_4" : [
        "fbff5bfe-ff9b-4aa4-8a51-b30ae33da3a8"
      ]
    },
    "119e43e7-c410-48dc-aa70-9f26d77649e4" : {
      "Rectangle_6" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Rectangle_7" : [
        "bb6a2cf1-cf7e-446c-838c-a667ec15a368"
      ],
      "Rectangle_8" : [
        "fbff5bfe-ff9b-4aa4-8a51-b30ae33da3a8"
      ]
    },
    "fbff5bfe-ff9b-4aa4-8a51-b30ae33da3a8" : {
      "Rectangle_2" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Rectangle_3" : [
        "bb6a2cf1-cf7e-446c-838c-a667ec15a368"
      ],
      "Rectangle_5" : [
        "f4a2babf-d3be-45cc-8019-b8dd67f27104"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "Rectangle_3" : [
        "bb6a2cf1-cf7e-446c-838c-a667ec15a368"
      ],
      "Rectangle_4" : [
        "fbff5bfe-ff9b-4aa4-8a51-b30ae33da3a8"
      ],
      "Rectangle_5" : [
        "f4a2babf-d3be-45cc-8019-b8dd67f27104"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);